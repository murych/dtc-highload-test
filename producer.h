#ifndef HIGHLOAD_PRODUCER_H
#define HIGHLOAD_PRODUCER_H

#include <QObject>
#include <memory>

namespace device {
class Thermocontrollers;
}

namespace transfer {
class Executor;
}

class QTimer;

namespace highload {

class Producer : public QObject
{
  Q_OBJECT

  enum class TemperaturePollerState
  {
    Idle,
    PollCurrentTemperature1,
    PollTargetTemperature1,
    PollCurrentTemperature2,
    PollTargetTemperature2
  };
  TemperaturePollerState m_tempPollerState{ TemperaturePollerState::Idle };
  void setTempPollerState(TemperaturePollerState state)
  {
    m_tempPollerState = state;
  }

  std::unique_ptr<device::Thermocontrollers> m_thermocontrollers{ nullptr };
  std::unique_ptr<transfer::Executor> m_executor{ nullptr };
  std::unique_ptr<QTimer> m_timer{ nullptr };

  int m_timeout{ 100 };

public:
  explicit Producer(QObject* parent = nullptr);
  ~Producer() override;

  [[nodiscard]] auto thermocontrollers() const
  {
    return m_thermocontrollers.get();
  }

  [[nodiscard]] auto executor() const { return m_executor.get(); }
  [[nodiscard]] auto timeout() const { return m_timeout; }

signals:
  void operationStarted();
  void operationFinished();
  void operationFailed(const QString& message);

  void timeoutChanged(int timeout);

  void started();
  void stopped();

public slots:
  void startPolling();
  void stopPolling();

  void setTimeout(int newTimeout);

  void getCurrentTemperature(uint8_t adr);
  void getTargetTemperature(uint8_t adr);

private slots:
  void doPollRoutine();

private:
  Q_PROPERTY(int timeout READ timeout WRITE setTimeout NOTIFY timeoutChanged)
};

} // namespace highload

#endif // HIGHLOAD_PRODUCER_H
