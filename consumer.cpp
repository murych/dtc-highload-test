#include "consumer.h"

namespace highload {

void
Consumer::setOperationCounter(uint64_t newOperationCounter)
{
  if (m_operationCounter == newOperationCounter)
    return;
  m_operationCounter = newOperationCounter;
  emit operationCounterChanged(m_operationCounter);
}

void
Consumer::setSuccessCounter(uint64_t newSuccessCounter)
{
  if (m_successCounter == newSuccessCounter)
    return;
  m_successCounter = newSuccessCounter;
  emit successCounterChanged(m_successCounter);
}

void
Consumer::setFailureCounter(uint64_t newFailureCounter)
{
  if (m_failureCounter == newFailureCounter)
    return;
  m_failureCounter = newFailureCounter;
  emit failureCounterChanged(m_failureCounter);
}

Consumer::Consumer(QObject* parent)
  : QObject{ parent }
{
}

Consumer::~Consumer() = default;

void
Consumer::onOperationStarted()
{
  setOperationCounter(operationCounter() + 1);
}

void
Consumer::onOperationFinished()
{
  setSuccessCounter(successCounter() + 1);
}

void
Consumer::onOperationFailed(const QString& reason)
{
  setFailureCounter(failureCounter() + 1);
  m_lastError = reason;
}

} // namespace highload
