#include "main.h"

#include "form.h"
#include <QApplication>
#include <QLibraryInfo>
#include <QStandardPaths>
#include <QsLog/QsLog.h>
#include <memory>

namespace {
const quint64 LOG_MAX_SIZE{ 100000000 };
const int LOG_MAX_OLD_COUNT{ 10 };
}

void
setup_names()
{
  QApplication::setOrganizationName(Main::CompanyName());
  QApplication::setOrganizationDomain(Main::CompanyDomain());
  QApplication::setApplicationName(QStringLiteral("DTC HIGHLOAD TEST"));
}

void
setup_logger(QsLogging::Logger* logger)
{
  logger->setLoggingLevel(QsLogging::TraceLevel);

  auto log_path{ QStandardPaths::writableLocation(
                   QStandardPaths::GenericDataLocation) +
                 QStringLiteral("/") + QApplication::organizationName() +
                 QStringLiteral("/") + QApplication::applicationName() +
                 QStringLiteral(".log") };
  auto file_destination{ QsLogging::DestinationFactory::MakeFileDestination(
    log_path,
    QsLogging::EnableLogRotation,
    QsLogging::MaxSizeBytes{ LOG_MAX_SIZE },
    QsLogging::MaxOldLogCount{ LOG_MAX_OLD_COUNT }) };
  logger->addDestination(file_destination);

  auto debug_destination(
    QsLogging::DestinationFactory::MakeDebugOutputDestination());
  logger->addDestination(debug_destination);
}

int
main(int argc, char* argv[])
{
  QApplication application{ argc, argv };

  setup_names();

  auto& logger{ QsLogging::Logger::instance() };
  setup_logger(&logger);

  QLOG_INFO().noquote() << "============"
                        << "APP" << QApplication::applicationName() << "STARTED"
                        << "============";
  QLOG_INFO().noquote() << "MAIN"
                        << "built with qt" << QLatin1String(QT_VERSION_STR)
                        << "running on" << QSysInfo::kernelType()
                        << QSysInfo::kernelVersion() << QLibraryInfo::version();
  QLOG_INFO() << "MAIN"
              << "built from" << BuildInfo::GitBranch() << "branch"
              << "commit" << BuildInfo::GitCommitHash() << "on"
              << BuildInfo::GitCommitTimestamp().toString(
                   QStringLiteral("yyyy-MM-dd/HH:mm"));

  Form form;
  form.show();

  auto ret_code{ QApplication::exec() };
  return ret_code;
}
