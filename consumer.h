#ifndef HIGHLOAD_CONSUMER_H
#define HIGHLOAD_CONSUMER_H

#include <QObject>

namespace highload {

class Consumer : public QObject
{
  Q_OBJECT

  uint64_t m_operationCounter{ 0 };
  uint64_t m_successCounter{ 0 };
  uint64_t m_failureCounter{ 0 };
  QString m_lastError{};

public:
  explicit Consumer(QObject *parent = nullptr);
  ~Consumer() override;

  auto operationCounter() const { return m_operationCounter; }
  auto successCounter() const { return m_successCounter; }
  auto failureCounter() const { return m_failureCounter; }

signals:
  void operationCounterChanged(uint64_t operationCounter);
  void successCounterChanged(uint64_t successCounter);
  void failureCounterChanged(uint64_t failureCounter);

public slots:
  void onOperationStarted();
  void onOperationFinished();
  void onOperationFailed(const QString& reason);

  void setOperationCounter(uint64_t newOperationCounter);
  void setSuccessCounter(uint64_t newSuccessCounter);
  void setFailureCounter(uint64_t newFailureCounter);

private:
  Q_PROPERTY(uint64_t operationCounter READ operationCounter WRITE
               setOperationCounter NOTIFY operationCounterChanged)
  Q_PROPERTY(uint64_t successCounter READ successCounter WRITE setSuccessCounter
               NOTIFY successCounterChanged)
  Q_PROPERTY(uint64_t failureCounter READ failureCounter WRITE setFailureCounter
               NOTIFY failureCounterChanged)
};

} // namespace highload

#endif // HIGHLOAD_CONSUMER_H
