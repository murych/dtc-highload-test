#include "form.h"
#include "ui_form.h"

#include "QsLog/QsLog.h"
#include "consumer.h"
#include "device/heater.h"
#include "device/thermocontroller.h"
#include "producer.h"

namespace {
const uint8_t HEATER1_ADDRESS{ 0x0f };
const uint8_t HEATER2_ADDRESS{ 0x10 };
}

Form::Form(QWidget* parent)
  : QWidget{ parent }
  , m_ui{ std::make_unique<Ui::Form>() }
  , m_producer{ std::make_unique<highload::Producer>() }
  , m_consumer{ std::make_unique<highload::Consumer>() }
{
  m_ui->setupUi(this);

  setupControls();

#ifdef TREADS_READY
  setupThreads();
#endif // THREADS_READY

  setupProducer();
  setupConsumer();

  setupSensors();
}

void
Form::setupControls()
{
  //  m_ui->lcdNumber->display(m_ui->pollRate->value());

  connect(this, &Form::stateChanged, this, [this](State new_state) {
    const auto enable{ new_state == State::Running };
    m_ui->start->setEnabled(!enable);
    m_ui->stop->setEnabled(enable);
  });
}

void
Form::setupProducer()
{
  connect(m_producer.get(), &highload::Producer::started, this, [this] {
    setState(State::Running);
  });
  connect(m_producer.get(), &highload::Producer::stopped, this, [this] {
    setState(State::Idle);
  });

  connect(m_ui->start,
          &QPushButton::clicked,
          m_producer.get(),
          &highload::Producer::startPolling);
  connect(m_ui->stop,
          &QPushButton::clicked,
          m_producer.get(),
          &highload::Producer::stopPolling);

  m_producer->setTimeout(m_ui->pollRate->value());
  connect(m_ui->pollRate,
          QOverload<int>::of(&QSpinBox::valueChanged),
          m_producer.get(),
          &highload::Producer::setTimeout);

  auto* thermocontrollers{ m_producer->thermocontrollers() };
  auto* heater1{ thermocontrollers->heater(HEATER1_ADDRESS) };
  auto* heater2{ thermocontrollers->heater(HEATER2_ADDRESS) };

  connect(m_ui->get_0f_current, &QPushButton::clicked, this, [=] {
    QLOG_INFO() << this << "REQUESTED" << heater1->address() << "CURRENT TEMP";
    m_producer->getCurrentTemperature(heater1->address());
  });
  connect(m_ui->get_10_current, &QPushButton::clicked, this, [=] {
    QLOG_INFO() << this << "REQUESTED" << heater2->address() << "CURRENT TEMP";
    m_producer->getCurrentTemperature(heater2->address());
  });
  connect(m_ui->get_0f_target, &QPushButton::clicked, this, [=] {
    QLOG_INFO() << this << "REQUESTED" << heater1->address() << "TARGET TEMP";
    m_producer->getTargetTemperature(heater1->address());
  });
  connect(m_ui->get_10_target, &QPushButton::clicked, this, [=] {
    QLOG_INFO() << this << "REQUESTED" << heater2->address() << "TARGET TEMP";
    m_producer->getTargetTemperature(heater2->address());
  });
}

void
Form::setupConsumer()
{
  connect(m_producer.get(),
          &highload::Producer::operationStarted,
          m_consumer.get(),
          &highload::Consumer::onOperationStarted);
  connect(m_producer.get(),
          &highload::Producer::operationFinished,
          m_consumer.get(),
          &highload::Consumer::onOperationFinished);
  connect(m_producer.get(),
          &highload::Producer::operationFailed,
          m_consumer.get(),
          &highload::Consumer::onOperationFailed);

  connect(m_consumer.get(),
          &highload::Consumer::operationCounterChanged,
          m_ui->executed,
          QOverload<int>::of(&QLCDNumber::display));
  connect(m_consumer.get(),
          &highload::Consumer::successCounterChanged,
          m_ui->success,
          QOverload<int>::of(&QLCDNumber::display));
  connect(m_consumer.get(),
          &highload::Consumer::failureCounterChanged,
          m_ui->failure,
          QOverload<int>::of(&QLCDNumber::display));
}

void
Form::setupSensors()
{
  auto* thermocontrollers{ m_producer->thermocontrollers() };
  auto* heater1{ thermocontrollers->heater(HEATER1_ADDRESS) };
  auto* heater2{ thermocontrollers->heater(HEATER2_ADDRESS) };

  connect(heater1,
          &device::Heater::currentTemperatureChanged,
          m_ui->heater1CurrentTemperatureDoubleSpinBox,
          &QDoubleSpinBox::setValue);
  connect(heater1,
          &device::Heater::targetTemperatureChanged,
          m_ui->heater1TargetTemperatureDoubleSpinBox,
          &QDoubleSpinBox::setValue);
  connect(heater2,
          &device::Heater::currentTemperatureChanged,
          m_ui->heater2CurrentTemperatureDoubleSpinBox,
          &QDoubleSpinBox::setValue);
  connect(heater2,
          &device::Heater::targetTemperatureChanged,
          m_ui->heater2TargetTemperatureDoubleSpinBox,
          &QDoubleSpinBox::setValue);
}

void
Form::setupThreads()
{
#ifdef THREADS_READY
  m_producer->moveToThread(&m_producerThread);
  m_producerThread.start();

  m_consumer->moveToThread(&m_consumerThread);
  m_consumerThread.start();
#endif // THREADS_READY
}

void
Form::setState(State new_state)
{
  m_state = new_state;
  emit stateChanged(m_state);
}

Form::~Form() = default;
