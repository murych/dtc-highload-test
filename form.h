#ifndef FORM_H
#define FORM_H

#include <QThread>
#include <QWidget>
#include <memory>

namespace highload {
class Consumer;
class Producer;
}

namespace Ui {
class Form;
}

class Form : public QWidget
{
  Q_OBJECT

  enum class State
  {
    Idle,
    Running
  };
  State m_state{ State::Idle };

public:
  explicit Form(QWidget *parent = nullptr);
  ~Form() override;

private:
  std::unique_ptr<Ui::Form> m_ui{ nullptr };

  std::unique_ptr<highload::Producer> m_producer{ nullptr };
  std::unique_ptr<highload::Consumer> m_consumer{ nullptr };

  void setupControls();

  void setupProducer();
  void setupConsumer();

  void setupSensors();

  void setupThreads();

  QThread m_producerThread;
  QThread m_consumerThread;

signals:
  void stateChanged(State state);

public slots:
  void setState(State new_state);
};

#endif // FORM_H
