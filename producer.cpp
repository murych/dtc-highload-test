#include "producer.h"

#include "device/heater.h"
#include "device/thermocontroller.h"
#include "transfer/executor.h"
#include "transfer/session_dtc.h"
#include <QTimer>

namespace {
// const std::chrono::milliseconds TIMEOUT{ 50 };
const uint8_t HEATER1_ADDRESS{ 0x0f };
const uint8_t HEATER2_ADDRESS{ 0x10 };
}

highload::Producer::Producer(QObject* parent)
  : m_thermocontrollers{ std::make_unique<device::Thermocontrollers>() }
  , m_executor{ std::make_unique<transfer::Executor>(
      nullptr,
      m_thermocontrollers.get()) }
  , m_timer{ std::make_unique<QTimer>() }
{
  m_thermocontrollers->setupSession("ttyUSB0");

  m_timer->setSingleShot(false);
  connect(m_timer.get(), &QTimer::timeout, this, &Producer::doPollRoutine);

  connect(m_executor.get(),
          &transfer::Executor::executionStarted,
          this,
          &Producer::operationStarted);
  connect(m_executor.get(),
          &transfer::Executor::executionFinished,
          this,
          &Producer::operationFinished);
  connect(m_executor.get(),
          &transfer::Executor::executionFailed,
          this,
          &Producer::operationFailed);
}

void
highload::Producer::startPolling()
{
  m_timer->start(m_timeout);
  emit started();
}

void
highload::Producer::stopPolling()
{
  m_timer->stop();
  emit stopped();
}

void
highload::Producer::doPollRoutine()
{
  switch (m_tempPollerState) {
    case TemperaturePollerState::Idle:
      setTempPollerState(TemperaturePollerState::PollCurrentTemperature1);
      break;
    case TemperaturePollerState::PollCurrentTemperature1:
      m_executor->getCurrentTemperature(
        m_thermocontrollers->heater(HEATER1_ADDRESS)->address());
      setTempPollerState(TemperaturePollerState::PollCurrentTemperature2);
      break;
    case TemperaturePollerState::PollTargetTemperature1:
      m_executor->getTargetTemperature(
        m_thermocontrollers->heater(HEATER1_ADDRESS)->address());
      setTempPollerState(TemperaturePollerState::PollTargetTemperature2);
      break;
    case TemperaturePollerState::PollCurrentTemperature2:
      m_executor->getCurrentTemperature(
        m_thermocontrollers->heater(HEATER2_ADDRESS)->address());
      setTempPollerState(TemperaturePollerState::PollTargetTemperature1);
      break;
    case TemperaturePollerState::PollTargetTemperature2:
      m_executor->getTargetTemperature(
        m_thermocontrollers->heater(HEATER2_ADDRESS)->address());
      setTempPollerState(TemperaturePollerState::PollCurrentTemperature1);
      break;
  }
}

highload::Producer::~Producer() = default;

void
highload::Producer::setTimeout(int new_timeout)
{
  if (m_timeout == new_timeout) {
    return;
  }
  m_timeout = new_timeout;
  m_timer->setInterval(m_timeout);
  emit timeoutChanged(m_timeout);
}

void
highload::Producer::getCurrentTemperature(uint8_t adr)
{
  m_executor->getCurrentTemperature(
    m_thermocontrollers->heater(adr)->address());
}

void
highload::Producer::getTargetTemperature(uint8_t adr)
{
  m_executor->getTargetTemperature(m_thermocontrollers->heater(adr)->address());
}
